from flask import *
from flask_httpauth import HTTPBasicAuth
from time import strftime
import logging
from logging.handlers import RotatingFileHandler
import traceback

app = Flask(__name__)
auth = HTTPBasicAuth()
@auth.get_password
def get_password(username):
    if username == 'usr':
        return 'psd'
    return None

students = [
    {
        'id':1,
        'name':'polly',
        'surname':'ngam',
        'dept':'eng(inter)'
    },
    {
        'id':2,
        'name':'nuppy',
        'surname':'umum',
        'dept':'eng(inter)'
    },
    {
        'id':3,
        'name':'somchai',
        'surname':'good',
        'dept':'sci'
    }
]

@app.route('/api/v1.0/students/',methods=['GET'])
def getStudents():
    return jsonify({'students': students})

@app.route('/api/v1.0/students/<int:student_id>/',methods=['GET'])
def getStudent(student_id):
    student = [student for student in students if student['id'] == student_id]
    if len(student) == 0:
        abort(404)

    return jsonify({'student':student[0]})

@app.route('/api/v1.0/students/',methods=['POST'])
def createStudent():
    if not request.json or not ('name' and 'surname' and 'dept') in request.json:
        abort(400)
    
    student = {
        'id': students[-1]['id'] + 1,
        'name':request.json['name'],
        'surname':request.json['surname'],
        'dept':request.json['dept']
    }
    students.append(student)
    return jsonify({'student':student}),201

@app.route('/api/v1.0/students/<int:student_id>/',methods=['PUT'])
@auth.login_required
def updateStudent(student_id):
    student = [student for student in students if student['id'] == student_id]
    if len(student) == 0:
        abort(404)

    if not request.json:
        abort(400)

    if not ('name' and 'surname' and 'dept') in request.json:
        abort(400)

    if (type(request.json['name']) and type(request.json['surname']) and type(request.json['dept'])) is not str:
        abort(400)

    student[0]['name'] = request.json.get('name',student[0]['name'])
    student[0]['surname'] = request.json.get('surname',student[0]['surname'])
    student[0]['dept'] = request.json.get('dept',student[0]['dept'])
    return jsonify({"student":student})

@app.route('/api/v1.0/students/<int:student_id>/',methods=['DELETE'])
@auth.login_required
def deleteStudent(student_id):
    student = [student for student in students if student['id'] == student_id]
    if len(student) == 0:
        abort(404)

    students.remove(student[0])
    return jsonify({"result":True}),400


#  logging and error handlers

@app.after_request
def after_request(response):
    timestamp = strftime('[%Y-%b-%d %H:%M]')
    logger.error('%s %s %s %s %s %s', timestamp, request.remote_addr, request.method, request.scheme, request.full_path, response.status)
    return response

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error':'Unauthorized access'}),401)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(Exception)
def exceptions(e):
    tb = traceback.format_exc()
    timestamp = strftime('[%Y-%b-%d %H:%M]')
    logger.error('%s %s %s %s %s 5xx INTERNAL SERVER ERROR\n%s', timestamp, request.remote_addr, request.method, request.scheme, request.full_path, tb)
    return e.status_code

if __name__ == '__main__':
    handler = RotatingFileHandler('app.log', maxBytes=100000, backupCount=3)
    logger = logging.getLogger('tdm')
    logger.setLevel(logging.ERROR)
    logger.addHandler(handler)
    app.run(debug=True)